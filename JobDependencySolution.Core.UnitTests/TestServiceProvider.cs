﻿using JobDependencySolution.Core.DependencyResolver;
using JobDependencySolution.Core.Parser;
using JobDependencySolution.Core.Validation;
using Microsoft.Extensions.DependencyInjection;

namespace JobDependencySolution.Core.UnitTests
{
    internal static class TestServiceProvider
    {
        private static readonly IServiceCollection serviceDescriptors;

        static TestServiceProvider()
        {
            serviceDescriptors = new ServiceCollection()
                                    .AddScoped<IJobDependencyResolver, JobDependencyResolver>()
                                    .AddScoped<IJobParser, ArrowSeparatedJobParser>()
                                    .AddScoped<IJobInputValidator, ArrowSeparatedJobInputValidator>();
        }

        public static ServiceProvider Instance => serviceDescriptors.BuildServiceProvider();
    }
}
