﻿using System.Collections.Generic;
using System.Linq;
using JobDependencySolution.Core.DependencyResolver;
using JobDependencySolution.Core.Exception;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace JobDependencySolution.Core.UnitTests.DependencyResolver
{
    internal class JobDependencyResolverTests : UnitTestBase
    {
        private IJobDependencyResolver jobDependencyResolver;

        [SetUp]
        public void Prepare()
        {
            this.jobDependencyResolver = ServiceProvider.GetService<IJobDependencyResolver>();
        }

        [Test]
        public void ResolveDependencies_InputSingleJob_ReturnSequenceWithSingleJobName()
        {
            // Arrange
            var data = TestData.JobDependencyDescriptionCollection.SingleJob;
            var expectedSequence = new Queue<char>(new[] { 'a' });

            // Act
            var result = this.jobDependencyResolver.ResolveDependencies(data);

            // Assert
            CollectionAssert.AreEqual(expectedSequence, result, "Single independent job should be resolved to a sequence with single character.");
        }

        [Test]
        public void ResolveDependencies_InputThreeIndependentJobs_ReturnSequenceWith3JobsNoSignificantOrder()
        {
            // Arrange
            var data = TestData.JobDependencyDescriptionCollection.ThreeIndependentJobs;
            var expectedSequence = new Queue<char>(new[] { 'a', 'b', 'c' });

            // Act
            var result = this.jobDependencyResolver.ResolveDependencies(data);

            // Assert
            CollectionAssert.AreEquivalent(expectedSequence, result, "Three independent jobs should be resolved to a sequence with 3 characters.");
        }

        [Test]
        public void ResolveDependencies_InputDependentJobs_ReturnSequenceWithSignificantOrder()
        {
            // Arrange
            var data = TestData.JobDependencyDescriptionCollection.DependentJobsData1;
            var expectedSequence = new Queue<char>(new[] { 'a', 'd', 'f', 'c', 'b', 'e' });

            // Act
            var result = this.jobDependencyResolver.ResolveDependencies(data).ToList();


            // Assert
            CollectionAssert.AreEquivalent(expectedSequence, result, "Input of independent jobs should be resolved to a sequence with all job characters.");

            Assert.IsTrue(result.IndexOf('c') < result.IndexOf('b') &&
                          result.IndexOf('f') < result.IndexOf('c') &&
                          result.IndexOf('a') < result.IndexOf('d') &&
                          result.IndexOf('b') < result.IndexOf('e'), "Predecessor jobs should be listed before its successors");
        }

        [Test]
        public void ResolveDependencies_InputJobsDependOnThemselves_ThrowException()
        {
            // Arrange
            var data = TestData.JobDependencyDescriptionCollection.JobsDependOnThemselves;

            // Act
            // Assert
            Assert.Throws<JobDependOnItselfException>(() => this.jobDependencyResolver.ResolveDependencies(data), "Input which has jobs depending on themselves should throw exception.");
        }

        [Test]
        public void ResolveDependencies_InputJobsCircularDependencies_ThrowException()
        {
            // Arrange
            var data = TestData.JobDependencyDescriptionCollection.CircularDependencies1;

            // Act
            // Assert
            Assert.Throws<JobCircularDependencyException>(() => this.jobDependencyResolver.ResolveDependencies(data), "Input which has circular dependencies should throw exception.");
        }
    }
}
