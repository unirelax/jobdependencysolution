﻿using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace JobDependencySolution.Core.UnitTests
{
    [TestFixture]
    internal class UnitTestBase
    {
        [SetUp]
        protected void BaseSetup()
        {
            this.ServiceProvider = TestServiceProvider.Instance;
        }

        [TearDown]
        protected void BaseTeardown()
        {
            this.ServiceProvider?.Dispose();
        }

        protected ServiceProvider ServiceProvider { get; set; }
    }
}
