﻿using JobDependencySolution.Core.Validation;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace JobDependencySolution.Core.UnitTests.Validation
{
    internal class ArrowSeparatedJobInputValidatorTests : UnitTestBase
    {
        private IJobInputValidator jobInputValidator;

        [SetUp]
        public void Prepare()
        {
            this.jobInputValidator = ServiceProvider.GetService<IJobInputValidator>();
        }

        [Test]
        public void InputValid_InputOnlyJobName_ReturnFalse()
        {
            // Arrange
            var jobDescription = TestData.JobInputs.SingleJobNameWithoutArrow;

            // Act
            var result = this.jobInputValidator.InputValid(jobDescription);

            // Assert
            Assert.IsFalse(result, "Validation should fail when input contains only the job name without arrow character.");
        }

        [Test]
        public void InputValid_InputOnlyPredecessorName_ReturnFalse()
        {
            // Arrange
            var jobDescription = TestData.JobInputs.JobOnlyWithPrecessorPart;

            // Act
            var result = this.jobInputValidator.InputValid(jobDescription);

            // Assert
            Assert.IsFalse(result, "Validation should fail when input contains only the predecessor job name.");
        }

        [Test]
        public void InputValid_InputJobNameWithMoreThanOneCharacter_ReturnFalse()
        {
            // Arrange
            var jobDescription = TestData.JobInputs.JobNameWithMoreThanOneCharacters;

            // Act
            var result = this.jobInputValidator.InputValid(jobDescription);

            // Assert
            Assert.IsFalse(result, "Validation should fail when input contains only the predecessor job name.");
        }

        [Test]
        public void InputValid_ValidInputOfIndependentJob_ReturnTrue()
        {
            // Arrange
            var jobDescription = TestData.JobInputs.ValidIndependentJobName;

            // Act
            var result = this.jobInputValidator.InputValid(jobDescription);

            // Assert
            Assert.IsTrue(result, "Validation should pass when input contains jobname and an arrow character.");
        }

        [Test]
        public void InputValid_ValidInputOfDependentJob_ReturnTrue()
        {
            // Arrange
            var jobDescription = TestData.JobInputs.ValidDependentJob;

            // Act
            var result = this.jobInputValidator.InputValid(jobDescription);

            // Assert
            Assert.IsTrue(result, "Validation should pass when input contains jobname its predecessor separated by an arrow.");
        }
    }
}
