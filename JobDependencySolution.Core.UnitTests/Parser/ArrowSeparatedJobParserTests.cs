﻿using System.Linq;
using JobDependencySolution.Core.Domain;
using JobDependencySolution.Core.Parser;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace JobDependencySolution.Core.UnitTests.Parser
{
    internal class ArrowSeparatedJobParserTests : UnitTestBase
    {
        private IJobParser jobParser;

        [SetUp]
        public void Prepare()
        {
            this.jobParser = ServiceProvider.GetService<IJobParser>();
        }

        [Test]
        public void ParseJobs_InputOnlyJobName_ReturnNoResult()
        {
            // Arrange
            var jobInput = new[] { TestData.JobInputs.SingleJobNameWithoutArrow };
            var expectedResult = Enumerable.Empty<JobDependencyDescription>();

            // Act
            var result = this.jobParser.ParseJobs(jobInput);

            // Assert
            CollectionAssert.AreEquivalent(expectedResult, result, "Input with only jobname should return no parsed records.");
        }

        [Test]
        public void ParseJobs_InputOnlyPredecessorName_ReturnNoResult()
        {
            // Arrange
            var jobInput = new[] { TestData.JobInputs.JobOnlyWithPrecessorPart };
            var expectedResult = Enumerable.Empty<JobDependencyDescription>();

            // Act
            var result = this.jobParser.ParseJobs(jobInput);

            // Assert
            CollectionAssert.AreEquivalent(expectedResult, result, "Input with only predecessor should return no parsed records.");
        }

        [Test]
        public void ParseJobs_InputJobNameWithMoreThanOneCharacter_ReturnNoResult()
        {
            // Arrange
            var jobInput = new[] { TestData.JobInputs.JobNameWithMoreThanOneCharacters };
            var expectedResult = Enumerable.Empty<JobDependencyDescription>();

            // Act
            var result = this.jobParser.ParseJobs(jobInput);

            // Assert
            CollectionAssert.AreEquivalent(expectedResult, result, "Input with jobname having more than 1 character should return no parsed records.");
        }


        [Test]
        public void ParseJobs_ValidInputOfIndependentJob_ReturnOneParsedRecord()
        {
            // Arrange
            var jobInput = new[] { TestData.JobInputs.ValidIndependentJobName };
            var expectedResult = new[] { new JobDependencyDescription('a', null) };

            // Act
            var result = this.jobParser.ParseJobs(jobInput);

            // Assert
            CollectionAssert.AreEquivalent(expectedResult, result, "Valid input of independent job should return 1 parsed record.");
        }

        [Test]
        public void ParseJobs_ValidInputOfDependentJob_ReturnOneParsedRecord()
        {
            // Arrange
            var jobInput = new[] { TestData.JobInputs.ValidDependentJob };
            var expectedResult = new[] { new JobDependencyDescription('a', 'b') };

            // Act
            var result = this.jobParser.ParseJobs(jobInput);

            // Assert
            CollectionAssert.AreEquivalent(expectedResult, result, "Valid input of independent job should return 1 parsed record.");
        }
    }
}
