﻿using System.Collections.Generic;
using JobDependencySolution.Core.Domain;

namespace JobDependencySolution.Core.UnitTests
{
    public static class TestData
    {
        public static class JobDependencyDescriptionCollection
        {
            public static List<JobDependencyDescription> SingleJob
            {
                get
                {
                    var jobs = new List<JobDependencyDescription>
                    {
                        new JobDependencyDescription('a', null)
                    };

                    return jobs;
                }
            }

            public static List<JobDependencyDescription> ThreeIndependentJobs
            {
                get
                {
                    var jobs = new List<JobDependencyDescription>
                    {
                        new JobDependencyDescription('a', null),
                        new JobDependencyDescription('b', null),
                        new JobDependencyDescription('c', null)
                    };

                    return jobs;
                }
            }

            public static List<JobDependencyDescription> DependentJobsData1
            {
                get
                {
                    var jobs = new List<JobDependencyDescription>
                    {
                        new JobDependencyDescription('b', 'c'),
                        new JobDependencyDescription('c', 'f'),
                        new JobDependencyDescription('d', 'a'),
                        new JobDependencyDescription('e', 'b'),
                        new JobDependencyDescription('f', null)
                    };

                    return jobs;
                }
            }

            public static List<JobDependencyDescription> JobsDependOnThemselves
            {
                get
                {
                    var jobs = new List<JobDependencyDescription>
                    {
                        new JobDependencyDescription('a', null),
                        new JobDependencyDescription('b', null),
                        new JobDependencyDescription('c', 'c')
                    };

                    return jobs;
                }
            }

            public static List<JobDependencyDescription> CircularDependencies1
            {
                get
                {
                    var jobs = new List<JobDependencyDescription>
                    {
                        new JobDependencyDescription('a', null),
                        new JobDependencyDescription('b', 'c'),
                        new JobDependencyDescription('c', 'f'),
                        new JobDependencyDescription('d', 'a'),
                        new JobDependencyDescription('e', null),
                        new JobDependencyDescription('f', 'b')
                    };

                    return jobs;
                }
            }

            public static List<JobDependencyDescription> CircularDependencies2
            {
                get
                {
                    var jobs = new List<JobDependencyDescription>
                    {
                        new JobDependencyDescription('b', 'a'),
                        new JobDependencyDescription('c', 'b'),
                        new JobDependencyDescription('b', 'c'),
                    };

                    return jobs;
                }
            }
        }

        public static class JobInputs
        {
            public const string SingleJobNameWithoutArrow = "a";

            public const string ValidIndependentJobName = "a=>";

            public const string ValidDependentJob = "a=>b";

            public const string JobOnlyWithPrecessorPart = "=>a";

            public const string JobNameWithMoreThanOneCharacters = "ab=>c";
        }
    }
}
