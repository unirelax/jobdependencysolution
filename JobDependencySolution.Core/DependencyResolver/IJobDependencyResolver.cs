﻿using System.Collections.Generic;
using JobDependencySolution.Core.Domain;

namespace JobDependencySolution.Core.DependencyResolver
{
    public interface IJobDependencyResolver
    {
        Queue<char> ResolveDependencies(IEnumerable<JobDependencyDescription> jobDependencyDescriptions);
    }
}
