﻿using System.Collections.Generic;
using System.Linq;
using JobDependencySolution.Core.Domain;
using JobDependencySolution.Core.Exception;

namespace JobDependencySolution.Core.DependencyResolver
{
    public class JobDependencyResolver : IJobDependencyResolver
    {
        public Queue<char> ResolveDependencies(IEnumerable<JobDependencyDescription> jobDependencyDescriptions)
        {
            if (jobDependencyDescriptions == null || !jobDependencyDescriptions.Any())
            {
                return new Queue<char>(0);
            }

            if (jobDependencyDescriptions.Any(e => char.Equals(e.Name, e.Predecessor)))
            {
                throw new JobDependOnItselfException();
            }

            var travelStack = new Stack<char>();
            var resultPlan = new Stack<char>();
            var allJobs = new List<char>();

            foreach (var e in jobDependencyDescriptions)
            {
                if (!allJobs.Contains(e.Name))
                {
                    allJobs.Add(e.Name);
                }

                if (e.Predecessor.HasValue && !allJobs.Contains(e.Predecessor.Value))
                {
                    allJobs.Add(e.Predecessor.Value);
                }
            }

            var dependentJobs = jobDependencyDescriptions.Where(e => e.Predecessor.HasValue).Select(e => e.Name).Distinct();
            var independentJobs = allJobs.Where(e => !dependentJobs.Contains(e)).Distinct();

            if (!independentJobs.Any())
            {
                throw new JobCircularDependencyException();
            }

            const bool found = true;
            var currentJob = default(char);
            var previousJob = default(char);

            var predecessorStat = jobDependencyDescriptions.Where(e => e.Predecessor.HasValue)
                                                           .Select(e => new PredecessorStat { JobName = e.Name, PredecessorName = e.Predecessor.Value })
                                                           .ToList();

            foreach (var ij in independentJobs)
            {
                travelStack.Push(ij);
                currentJob = ij;

                while (found)
                {
                    var nextSuccessor = predecessorStat.FirstOrDefault(s => char.Equals(s.PredecessorName, currentJob) && !char.Equals(s.CheckedBy, ij));

                    if (nextSuccessor != null)
                    {
                        if (travelStack.Contains(nextSuccessor.JobName))
                        {
                            throw new JobCircularDependencyException();
                        }

                        travelStack.Push(nextSuccessor.JobName);
                        currentJob = nextSuccessor.JobName;
                        continue;
                    }

                    if (!resultPlan.Contains(currentJob))
                    {
                        resultPlan.Push(currentJob);
                    }

                    currentJob = travelStack.Pop();

                    if (travelStack.Count == 0)
                    {
                        break;
                    }

                    previousJob = travelStack.Peek();

                    // Mark checked by current independent job.
                    predecessorStat.FirstOrDefault(s => char.Equals(s.PredecessorName, previousJob) && char.Equals(s.JobName, currentJob)).CheckedBy = ij;
                    currentJob = previousJob;
                }

                travelStack.Clear();
            }

            // Input contains sub circular dependencies that have not been processed.
            if (resultPlan.Count != allJobs.Count)
            {
                throw new JobCircularDependencyException();
            }

            return new Queue<char>(resultPlan);
        }

        private class PredecessorStat
        {
            public char JobName { get; set; }

            public char PredecessorName { get; set; }

            public char CheckedBy { get; set; }
        }
    }
}
