﻿namespace JobDependencySolution.Core.Exception
{
    public class JobCircularDependencyException : System.Exception
    {
        public override string Message => "Jobs cannot have circular dependencies.";
    }
}
