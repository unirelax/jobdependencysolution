﻿namespace JobDependencySolution.Core.Exception
{
    public class JobDependOnItselfException : System.Exception
    {
        public override string Message => "Jobs cannot depend on themselves.";
    }
}
