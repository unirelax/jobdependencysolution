﻿using System.Text.RegularExpressions;

namespace JobDependencySolution.Core.Validation
{
    public class ArrowSeparatedJobInputValidator : IJobInputValidator
    {
        private static readonly Regex RegEx = new Regex(@"^\s*[a-z]{1}\s*(=>){1}\s*[a-z]{0,1}\s*$", RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Compiled);

        public bool InputValid(string jobInput)
        {
            return RegEx.IsMatch(jobInput);
        }
    }
}
