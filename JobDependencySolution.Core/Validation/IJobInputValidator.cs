﻿namespace JobDependencySolution.Core.Validation
{
    public interface IJobInputValidator
    {
        bool InputValid(string jobInput);
    }
}
