﻿using System.Collections.Generic;
using JobDependencySolution.Core.Domain;

namespace JobDependencySolution.Core.Parser
{
    public interface IJobParser
    {
        IEnumerable<JobDependencyDescription> ParseJobs(IEnumerable<string> inputJobs);
    }
}
