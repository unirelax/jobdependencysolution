﻿using System;
using System.Collections.Generic;
using System.Linq;
using JobDependencySolution.Core.Domain;
using JobDependencySolution.Core.Exception;
using JobDependencySolution.Core.Validation;

namespace JobDependencySolution.Core.Parser
{
    public class ArrowSeparatedJobParser : IJobParser
    {
        private readonly IJobInputValidator jobInputValidator;

        public ArrowSeparatedJobParser(IJobInputValidator jobInputValidator)
        {
            this.jobInputValidator = jobInputValidator;
        }

        public IEnumerable<JobDependencyDescription> ParseJobs(IEnumerable<string> inputJobs)
        {
            if (inputJobs == null || !inputJobs.Any())
            {
                yield break;
            }

            foreach (var input in inputJobs)
            {
                var inputValid = this.jobInputValidator.InputValid(input);

                if (!inputValid)
                {
                    continue;
                }

                var parts = input.Split(new[] { "=>" }, StringSplitOptions.RemoveEmptyEntries).Select(e => char.Parse(e.Trim()));

                if (parts == null)
                {
                    continue;
                }

                switch (parts.Count())
                {
                    default:
                        {
                            continue;
                        }

                    case 1:
                        {
                            var newJob = new JobDependencyDescription(parts.ElementAt(0), null);
                            yield return newJob;
                            continue;
                        }

                    case 2:
                        {
                            if (char.Equals(parts.ElementAt(0), parts.ElementAt(1)))
                            {
                                throw new JobDependOnItselfException();
                            }

                            var newJob = new JobDependencyDescription(parts.ElementAt(0), parts.ElementAt(1));
                            yield return newJob;
                            continue;
                        }
                }
            }
        }
    }
}
