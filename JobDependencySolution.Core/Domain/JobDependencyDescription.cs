﻿using System;

namespace JobDependencySolution.Core.Domain
{
    public class JobDependencyDescription : IComparable<JobDependencyDescription>, IEquatable<JobDependencyDescription>
    {
        public JobDependencyDescription(char jobName, char? Predecessor)
        {
            this.Name = jobName;
            this.Predecessor = Predecessor;
        }

        public char? Predecessor { get; }

        public char Name { get; }

        public int CompareTo(JobDependencyDescription other)
        {
            return Compare(this, other);
        }

        public bool Equals(JobDependencyDescription other)
        {
            var equal = this.Compare(this, other);
            return equal == 0;
        }

        private int Compare(JobDependencyDescription x, JobDependencyDescription y)
        {
            if (x == null && y == null)
            {
                // Equals.
                return 0;
            }

            if ((x == null && y != null) || (x != null && y == null))
            {
                return 1;
            }

            var equal = char.Equals(x.Name, y.Name) && char.Equals(x.Predecessor, y.Predecessor);

            return equal ? 0 : 1;
        }
    }
}
