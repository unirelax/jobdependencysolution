﻿using System;
using System.Collections.Generic;
using System.Linq;
using JobDependencySolution.Core.DependencyResolver;
using JobDependencySolution.Core.Domain;
using JobDependencySolution.Core.Parser;
using JobDependencySolution.Core.Validation;

namespace ConsoleApp
{
    internal class AppInteraction : IAppInteraction
    {
        private readonly IJobDependencyResolver jobDependencyResolver;
        private readonly IJobParser jobParser;
        private readonly IJobInputValidator jobInputValidator;

        private readonly Dictionary<ConsoleKey, Action> handledOperations;
        private readonly List<JobDependencyDescription> jobs;

        public AppInteraction(IJobDependencyResolver jobDependencyResolver, IJobParser jobParser, IJobInputValidator jobInputValidator)
        {
            this.jobDependencyResolver = jobDependencyResolver;
            this.jobParser = jobParser;
            this.jobInputValidator = jobInputValidator;

            handledOperations = new Dictionary<ConsoleKey, Action>
            {
                { ConsoleKey.A, HandleJobInput },
                { ConsoleKey.S, HandleShowAllJobs },
                { ConsoleKey.R, HandleResolveJobDependencies },
            };

            this.jobs = new List<JobDependencyDescription>();
        }

        public void Start()
        {
            ConsoleKeyInfo consoleKeyInfo = default(ConsoleKeyInfo);

            do
            {
                PrintOptions();
                consoleKeyInfo = Console.ReadKey();

                if (consoleKeyInfo.Key == ConsoleKey.Escape)
                {
                    ConsolePrint(Environment.NewLine);
                    break;
                }

                if (!handledOperations.ContainsKey(consoleKeyInfo.Key))
                {
                    ConsolePrint(Environment.NewLine);
                    ConsolePrint("This is not a supported feature!");
                    continue;
                }

                var registeredHandler = handledOperations[consoleKeyInfo.Key];

                ConsolePrint(Environment.NewLine);
                registeredHandler();
            }
            while (true);
        }

        private void HandleResolveJobDependencies()
        {
            try
            {
                var resultPlan = this.jobDependencyResolver.ResolveDependencies(this.jobs);

                ConsolePrint(string.Join(",", resultPlan));
            }
            catch (Exception ex)
            {
                ConsolePrint(ex.Message);
            }
        }

        private void HandleShowAllJobs()
        {
            foreach (var e in this.jobs)
            {
                ConsolePrint($"{e.Name} => {e.Predecessor}");
            }
        }

        private void HandleJobInput()
        {
            ConsolePrint("Enter job and its predecessor. Syntax: Job => Precessor");
            ConsolePrint($"Press {ConsoleKey.Enter.ToString()} without any input will return to the main menu");

            string inputJob;
            var jobsInput = new List<string>();

            while (!string.IsNullOrWhiteSpace(inputJob = Console.ReadLine()))
            {
                var inputValid = this.jobInputValidator.InputValid(inputJob);

                if (!inputValid)
                {
                    ConsolePrint("Your input is not valid");
                    continue;
                }

                jobsInput.Add(inputJob);
            }

            var parsedJobs = this.jobParser.ParseJobs(jobsInput);

            foreach (var j in parsedJobs)
            {
                if (this.jobs.Any(e => char.Equals(e.Name, j.Name)))
                {
                    continue;
                }

                this.jobs.Add(j);
            }
        }

        private bool JobInputValid(string job)
        {
            return true;
        }

        private static void PrintOptions()
        {
            ConsolePrint($"Press {ConsoleKey.A.ToString()} to add jobs");
            ConsolePrint($"Press {ConsoleKey.S.ToString()} to show all jobs input");
            ConsolePrint($"Press {ConsoleKey.R.ToString()} to resolve job dependencies");
            ConsolePrint($"Press {ConsoleKey.Escape.ToString()} to quit");

            ConsolePrint($"Your option:", newLine: false);
        }

        private static void ConsolePrint(string text, bool newLine = true)
        {
            if (newLine)
            {
                Console.WriteLine(text);
            }
            else
            {
                Console.Write(text);
            }
        }
    }
}
