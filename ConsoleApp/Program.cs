﻿using JobDependencySolution.Core.DependencyResolver;
using JobDependencySolution.Core.Parser;
using JobDependencySolution.Core.Validation;
using Microsoft.Extensions.DependencyInjection;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var serviceProvider = RegisterServices())
            {
                var appInteraction = serviceProvider.GetService<IAppInteraction>();
                appInteraction.Start();
            }
        }

        static ServiceProvider RegisterServices()
        {
            var serviceProvider = new ServiceCollection()
                                    .AddScoped<IAppInteraction, AppInteraction>()
                                    .AddScoped<IJobDependencyResolver, JobDependencyResolver>()
                                    .AddScoped<IJobParser, ArrowSeparatedJobParser>()
                                    .AddScoped<IJobInputValidator, ArrowSeparatedJobInputValidator>()
                                    .BuildServiceProvider();

            return serviceProvider;
        }
    }
}